﻿<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
//$doc->addScript('templates/' . $this->template . '/js/jquery-ui-1.8.17.custom.min.js');
$doc->addScript('templates/' . $this->template . '/js/template.js');
$doc->addScript('templates/' . $this->template . '/js/waypoints.min.js');
$doc->addScript('templates/' . $this->template . '/js/carusel/jquery.carouFredSel-5.2.3.js');

// Add Stylesheets
$doc->addStyleSheet('templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/style.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);


// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle')) . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i,cid){w[l]=w[l]||[];w.pclick_client_id=cid;w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:''; j.async=true; j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P23G9N', '63077');</script>
<!-- End Google Tag Manager -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="google-site-verification" content="7CCqqhhE78P1CKj9m4RI5ahgo8pINkMVnzMLNKm-ghE" />
	<meta name="yandex-verification" content="d6e6a37d30701565" />
    <meta name='yandex-verification' content='76b5e5083cde4256' />
	<jdoc:include type="head" />
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->
    <script type="text/javascript" src="<?php echo 'templates/' . $this->template . '/js/transfers.js';?>"></script>
</head>
<?php if($_SERVER['REQUEST_URI'] == "/" or  $_SERVER['REQUEST_URI'] == "/index.php") {  // шаблон для главной страницы
?>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P23G9N"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->

<div id="wrapper">
  <header id="header">
  <div class="logo"><jdoc:include type="modules" name="logo" style="xhtml" /></div><!--.logo-->
    <div class="slogan">  <jdoc:include type="modules" name="slogan" style="xhtml" /></div><!--.slogan-->
    <div class="phone"> 
    <div class="tel1"><jdoc:include type="modules" name="tel1" style="xhtml" /></div> 
    <div class="tel2"><jdoc:include type="modules" name="tel2" style="xhtml" /></div> 
    <div class="email"><jdoc:include type="modules" name="email" /></div>
    </div>
    <div class="clear"></div>
    <div class="search_axeld">
      <jdoc:include type="modules" name="search" style="xhtml"/>
    </div><!--.search_axeld-->
    <div class="all_search">
    <div class="all_search_title">Уточнить ?</div>    
      <div class="all_search_">
       <jdoc:include type="modules" name="all_search" style="xhtml"/>
     </div><!--.all_search_-->
    </div><!--.all_search-->
    <div class="clear"></div>
  </header><!-- #header-->
    <div class="clear"></div>
    <div class="top_menu">
      <!-- jot top_menu s style="xhtml" --><jdoc:include type="modules" name="top_menu" style="xhtml" /><!-- jot top_menu e -->
    </div><!--top_menu-->
    <div class="clear"></div>
    <div class="slider"> 
     <jdoc:include type="modules" name="slider" style="xhtml" />
    </div><!--.slider-->
        <div class="podstava"></div><!--.podstava-->

    <div class="wrap1">
         <?php if($this->countModules('action')) : ?>
        <jdoc:include type="modules" name="action" style="xhtml" />     
        <?php endif; ?>
      <?php if($this->countModules('index_cat')) : ?>
            <jdoc:include type="modules" name="index_cat"/>
        <?php endif; ?> 
        <div class="clear"></div>
 
    <div class="wrap2">
    <div class="content_index">
        <?php if($this->countModules('user1')) : ?>
            <jdoc:include type="modules" name="user1" style="xhtml" />
        <?php endif; ?> 
        <?php if($this->countModules('user2')) : ?>
            <jdoc:include type="modules" name="user2" style="xhtml" />
        <?php endif; ?> 
        
                     <jdoc:include type="component" />
        
        <?php if($this->countModules('user3')) : ?>
            <jdoc:include type="modules" name="user3" style="xhtml" />
        <?php endif; ?> 
        <?php if($this->countModules('user4')) : ?>
            <jdoc:include type="modules" name="user4" style="xhtml" />
        <?php endif; ?>


      <?php if($this->countModules('part')) : ?>
            <jdoc:include type="modules" name="part" style="xhtml" />
        <?php endif; ?> 
    </div><!--.content_index-->
    <div class="content_right">
    
      <?php if($this->countModules('index_news')) : ?>
            <jdoc:include type="modules" name="index_news" style="xhtml" />
        <?php endif; ?>
    
    </div><!--.content_right-->
    
  </div><!--.wrap2-->
    <div class="clear"></div>
  
    <div class="wrap3">


            
            
      <?php if($this->countModules('index_product')) : ?>
            <jdoc:include type="modules" name="index_product"/>
        <?php endif; ?>
   
   
  </div><!--.wrap3-->    
  </div><!--.wrap1-->
        <div class="clear"></div>

<div class="oferta"> <div>Обращаем ваше внимание на то, что данный интернет-сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 (2) Гражданского кодекса Российской Федерации.<br>
 Для получения подробной информации о наличии и стоимости указанных товаров и (или) услуг, пожалуйста, обращайтесь к менеджерам отдела клиентского обслуживания с помощью специальной формы связи или по телефону 642-56-01</div>
 </div>

</div><!-- #wrapper -->
  <footer id="footer">
        <?php if($this->countModules('fixetdiv')) : ?>
            <jdoc:include type="modules" name="fixetdiv"  />
        <?php endif; ?>

    <div class="foot">
    <div class="f_amy10">
      <?php if($this->countModules('liveinternet')) : ?>
            <jdoc:include type="modules" name="liveinternet" style="xhtml" />
        <?php endif; ?>
        </div>
    
    <div class="f_amy11">   
      
    <?php if($this->countModules('adress')) : ?>
            <jdoc:include type="modules" name="adress"  />
        <?php endif; ?>
    
    </div>
    
    <div class="f_amy12">
    <div class="Spb-Webmaster">

  <?php if($this->countModules('Spb-Webmaster_index')) {?>
            <jdoc:include type="modules" name="Spb-Webmaster_index"  />
            <?php } else {          
          
                    if($this->countModules('Sitemega')) {?>
                    <jdoc:include type="modules" name="Sitemega"  />
                    <?php } else { 
                            if($this->countModules('Axeld')) {?>
                            <jdoc:include type="modules" name="Axeld"  />
                            <? } else { ?>
                   
                        <?php     }  
                   
                       }
      
                   } 
        ?> 
</div>
    <div class="copy">
      <?php if($this->countModules('copy')) : ?>
            <jdoc:include type="modules" name="copy"  />
        <?php endif; ?></div></div></div><!--.foot-->
  </footer><!-- #footer -->

    <script>
              setTimeout(function() {jQuery('.fixetdiv').show(1000); }, 2000);
        </script>

<script>
setTimeout(function() {jQuery('.fixetdiv').show(1000); }, 2000);
jQuery('.menu_button').css({'display':'none'});
	  
  jQuery('.mar').waypoint(function() {
	    jQuery('.menu_button').css({'position':'fixed'});
        jQuery('.menu_button').fadeIn();
    });

    jQuery(window).scroll(function() {
        var waypoint = jQuery("body");
        var scrollTop = jQuery(window).scrollTop();

        // change to active 0-10 pixels
        if (scrollTop >= waypoint.offset().top && scrollTop <= waypoint.offset().top) {
            jQuery('.menu_button').fadeOut();

        }
    });	  	  
 			
	jQuery('.menuabsol').click(function() {jQuery('#sidebar2 .zakaz-zvonka02').slideDown(1000);});
	jQuery('.close_form02').click(function() {jQuery('#sidebar2 .zakaz-zvonka02').slideUp(1000);});					   
 




</script>
   
</body>


<?php } // конец шаблона для главной странице
else {
?>







<body>
<div class="cart_wi100"></div>
<div id="wrapper">
  <header id="header">
  <div class="logo"><a href="/"><jdoc:include type="modules" name="logo" style="xhtml" /></a></div><!--.logo-->
    <div class="slogan">  <jdoc:include type="modules" name="slogan" style="xhtml" /></div><!--.slogan-->
    <div class="phone"> 
    <div class="tel1"><jdoc:include type="modules" name="tel1" style="xhtml" /></div> 
    <div class="tel2"><jdoc:include type="modules" name="tel2" style="xhtml" /></div> 
    <div class="email"><jdoc:include type="modules" name="email" /></div>
    </div>
    <div class="clear"></div>
    <div class="search_axeld">
      <jdoc:include type="modules" name="search" style="xhtml"/>
    </div><!--.search_axeld-->
    <div class="all_search">
    <div class="all_search_title">Уточнить ?</div>    
      <div class="all_search_">
       <jdoc:include type="modules" name="all_search" style="xhtml"/>
     </div><!--.all_search_-->
    </div><!--.all_search-->
    <div class="clear"></div>  
  </header><!-- #header-->
    <div class="clear"></div>
    <div class="top_menu">
   <!-- jot top_menu s style="xhtml" --><jdoc:include type="modules" name="top_menu" style="xhtml" /><!-- jot top_menu e -->
    </div><!--top_menu-->
    <div class="clear"></div>
   
    
                  <?php if($this->countModules('slider')) : ?>
   
                        <div class="slider">
                         <jdoc:include type="modules" name="slider" style="xhtml" />
                        </div><!--.slider-->
                            <div class="podstava"></div><!--.podstava-->
                            <div class="podstava2"></div><!--.podstava2-->
                 <?php endif; ?> 
     
    <div class="wrap1">
 
    <div class="wrap2">
     <div class="content_left">
       <div class="left_act">
        <?php if($this->countModules('action')) : ?>
        <jdoc:include type="modules" name="action" style="xhtml" />     
        <?php endif; ?>
       </div><!--.left_act-->
       <div class="class-dlya-action">
       
        <div class="pusto_" id="navigation-cart">
        <?php if($this->countModules('navigation-cart')) : ?>
            <jdoc:include type="modules" name="navigation-cart" style="xhtml" />
        <?php endif; ?>      
        </div><!--#navigation-cart-->
       
        <?php if($this->countModules('left_bar1')) : ?>
            <jdoc:include type="modules" name="left_bar1" style="xhtml" />
        <?php endif; ?>
         <div class="clear"></div>    
        <?php if($this->countModules('left_bar2')) : ?>
            <jdoc:include type="modules" name="left_bar2" style="xhtml" />
        <?php endif; ?>
        <div class="clear"></div>
     <?php if($this->countModules('index_news')) : ?>
                <div class="page_news"><jdoc:include type="modules" name="index_news" style="xhtml" /></div>
         <?php endif; ?>
        <div class="clear"></div>
     <?php if($this->countModules('left_bar3')) : ?>
                <jdoc:include type="modules" name="left_bar3" style="xhtml" />
        <?php endif; ?>
    </div><!--.class-dlya-action-->
    
    </div><!--.content_left-->

        <?php if($this->countModules('user3')) : ?>
            <style>
                .moduletable .breadcrumb {
                    border: 1px solid #d9d9d9;
                    border-bottom: none;
                    border-radius: 0;
                    margin-left: 1px;
                    margin-right: 1px;
                }
            </style>
            <jdoc:include type="modules" name="user3" style="xhtml" />
        <?php endif; ?>

        <div class="content_page">

    <?php if($this->countModules('user1')) : ?>
            <jdoc:include type="modules" name="user1" style="xhtml" />
        <?php endif; ?> 
        <?php if($this->countModules('user2')) : ?>
            <jdoc:include type="modules" name="user2" style="xhtml" />
        <?php endif; ?> 
        
                     <jdoc:include type="component" />
        
<!--        --><?php //if($this->countModules('user3')) : ?>
<!--            <jdoc:include type="modules" name="user3" style="xhtml" />-->
<!--        --><?php //endif; ?><!-- -->

        <?php if($this->countModules('user4')) : ?>
            <jdoc:include type="modules" name="user4" style="xhtml" />
        <?php endif; ?>


    </div><!--.content_page-->
   
  </div><!--.wrap2-->
    <div class="clear"></div>
  
      <?php if($this->countModules('wrap3')) : ?>
    <div class="wrap3">
            <jdoc:include type="modules" name="wrap3" style="xhtml" />
  
  </div><!--.wrap3-->
        <?php endif; ?> 


      
  </div><!--.wrap1-->
        <div class="clear"></div>

<div class="oferta"> <div>Обращаем ваше внимание на то, что данный интернет-сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 (2) Гражданского кодекса Российской Федерации.<br>
 Для получения подробной информации о наличии и стоимости указанных товаров и (или) услуг, пожалуйста, обращайтесь к менеджерам отдела клиентского обслуживания с помощью специальной формы связи или по телефону 642-56-01</div>
 </div>


</div><!-- #wrapper -->
    <footer id="footer">
        <?php if($this->countModules('fixetdiv')) : ?>
            <jdoc:include type="modules" name="fixetdiv"  />
        <?php endif; ?>

    <div class="foot">
    <div class="f_amy10">
      <?php if($this->countModules('liveinternet')) : ?>
            <jdoc:include type="modules" name="liveinternet" style="xhtml" />
        <?php endif; ?>
        </div>
    
    <div class="f_amy11">   
      
    <?php if($this->countModules('adress')) : ?>
            <jdoc:include type="modules" name="adress"  />
        <?php endif; ?>
    
    </div>
    
    <div class="f_amy12">
    <div class="Spb-Webmaster">

  <?php if($this->countModules('Spb-Webmaster_index')) {?>
            <jdoc:include type="modules" name="Spb-Webmaster_index"  />
            <?php } else {          
          
                    if($this->countModules('Sitemega')) {?>
                    <jdoc:include type="modules" name="Sitemega"  />
                    <?php } else { 
                            if($this->countModules('Axeld')) {?>
                            <jdoc:include type="modules" name="Axeld"  />
                            <? } else { ?>
                             <jdoc:include type="modules" name="Spb-Webmaster"  />  
                        <?php     }  
                   
                       }
      
                   } 
        ?> 
</div>
    <div class="copy">
      <?php if($this->countModules('copy')) : ?>
            <jdoc:include type="modules" name="copy"  />
        <?php endif; ?></div></div></div><!--.foot-->
  </footer><!-- #footer -->
        <script type="text/javascript" src="/templates/tdartelnew/js/jquery.scrollTo-min.js"></script>

   
    <script>

	
	
      jQuery(document).ready(function(){
      
      jQuery('.backk').click(function() {
        jQuery('.moduletable-vop').show(1000);
        //str = document.getElementById('anchor1');
        jQuery.scrollTo(".moduletable-vop",1000);
        
      });
      jQuery('.priceartel_bot').click(function() {
        //str = document.getElementById('anchor1');
        jQuery.scrollTo(".priceartel",1000);
        
      });
      
              
      });
    </script>         
    <script>
              setTimeout(function() {jQuery('.fixetdiv').show(1000); }, 2000);
			                         jQuery('.my_variable2').show();
        </script>
        <script>
        if(jQuery(".div_tdartel_custom table").length>0) { 
      //html элемент существует 
      //alert("");
      jQuery(".div_tdartel_custom table").addClass("table table-hover table-bordered table-striped table-price");
           }
    jQuery(".table-price tr").each(function(i) {
      jQuery(".table-price tr td").removeAttr('width').removeAttr('height');
      jQuery(".table-price").removeAttr('style');

         //alert(i + ': ' + $(this).text());
     $(this).addClass("trprice"+i);
     if(i>11){$(this).addClass("displaynone");}
     
     
          });
         var trprice11Html = jQuery(".trprice11").html();
     var trprice11 = jQuery(".trprice11");
     trprice11.html('<td colspan="4" class="priceact">Смотреть весь прайс</td>');
     trprice11.click(function(){
     trprice11.html(trprice11Html);
    // jQuery(".displaynone").addClass("displayblock").removeClass("displaynone");
     jQuery(".displaynone").show();  
     jQuery(".priceartel_bot").show(); 
       });
     
              //setTimeout(function() {jQuery('.fixetdiv').show(1000); }, 2000);
        </script>
    <script>
        jQuery(".z01").click(function(){
        jQuery(".z01").removeClass("z03");
        jQuery(".z02").addClass("z03");  
        
    jQuery(".poll_name").hide(500);
    jQuery(".poll_inn").hide(500);
    jQuery(".poll_kpp").hide(500);
    jQuery(".poll_adress").hide(500);

    
        });
        /////
        
        jQuery(".z02").click(function(){
        jQuery(".z02").removeClass("z03");
        jQuery(".z01").addClass("z03");  
      jQuery(".poll_name").show(500);
    jQuery(".poll_inn").show(500);
    jQuery(".poll_kpp").show(500);
    jQuery(".poll_adress").show(500);
        
            });  

            var height = jQuery("div.content_page").height();
      //alert(height);
            if(height<1000) {
            
                    jQuery(".j_hei .j_news").each(function(i) { 
                    
                    if(i>2) {
                    jQuery(this).addClass("displaynone");  
                             }
                        });
            }

if(jQuery(".jquery_st").length>0) {
  
  
                         

                     jQuery(".jquery_st ul li a").each(function(i) { 
                    // jQuery(this).addClass("siva");
           
           
      
               var str = jQuery(this).text();
               var str2 = str.substr(0,30);
               jQuery(this).text(str2);  
               // Создаем элемент DIV ser02
              
              var Myhtml = jQuery(this).html();
              jQuery(this).html('<div class="ser01 dis'+i+'">'+Myhtml+'</div><div class="ser02"></div>');
                  
                
                                     
                                                        });

                                   }
	 jQuery(".item-page img").each(function(){
        var y = jQuery(this).attr('align');
		if(y=="left") {jQuery(this).addClass("dsdsds");}
		if(y=="right") {jQuery(this).addClass("dsdsds2");}
		
      });
	 jQuery(".item-page-newsbilding img").each(function(){
        var y = jQuery(this).attr('align');
		if(y=="left") {jQuery(this).addClass("dsdsds");}
		if(y=="right") {jQuery(this).addClass("dsdsds2");}
		
      });
  jQuery('.menu_button').css({'display':'none'});
	  
  jQuery('.mar').waypoint(function() {
	    jQuery('.menu_button').css({'position':'fixed'});
		jQuery('.menualex').css({'position':'fixed'}); 
								jQuery('.menualex').css({'top':'87px'}); 
        jQuery('.menu_button').fadeIn();
    });

    jQuery(window).scroll(function() {
        var waypoint = jQuery("body");
        var scrollTop = jQuery(window).scrollTop();

        // change to active 0-10 pixels
        if (scrollTop >= waypoint.offset().top && scrollTop <= waypoint.offset().top) {
            jQuery('.menu_button').fadeOut();
			jQuery('.menualex').css({'position':'relative'}); 
						jQuery('.menualex').css({'top':'0'}); 


        }
    });	  	  
 			
	jQuery('.menuabsol').click(function() {jQuery('#sidebar2 .zakaz-zvonka02').slideDown(1000);});
	jQuery('.close_form02').click(function() {jQuery('#sidebar2 .zakaz-zvonka02').slideUp(1000);});					   
     
	 //////////////
	    if(jQuery(".menualex").length>0) {
			
			jQuery(".class-dlya-action div").addClass("prozra");
			} 
	 
	 jQuery(".r01").click(function(){
		  jQuery(".r01_act").show();
		  jQuery(".r02_act").hide();
		  jQuery(".r03_act").hide();
		  jQuery(".r04_act").hide();
		  jQuery(".r05_act").hide();
			jQuery(".r06_act").hide();
		    jQuery(".r07_act").hide();
			jQuery(".r08_act").hide();		  
		  //
		  jQuery(".r01").addClass("r_active");
		  jQuery(".r02").removeClass("r_active");
		  jQuery(".r03").removeClass("r_active");
		  jQuery(".r04").removeClass("r_active");
    	  jQuery(".r05").removeClass("r_active");
    	  jQuery(".r06").removeClass("r_active");
    	  jQuery(".r07").removeClass("r_active");
    	  jQuery(".r08").removeClass("r_active");		  
		 });
		 jQuery(".r02").click(function(){
		  jQuery(".r02_act").show();
		  jQuery(".r01_act").hide();
		  jQuery(".r03_act").hide();
		  jQuery(".r04_act").hide();
		  jQuery(".r05_act").hide();
			jQuery(".r06_act").hide();
		    jQuery(".r07_act").hide();
			jQuery(".r08_act").hide();
		  //
		  jQuery(".r02").addClass("r_active");
		  jQuery(".r01").removeClass("r_active");
		  jQuery(".r03").removeClass("r_active");
		  jQuery(".r04").removeClass("r_active");
    	  jQuery(".r05").removeClass("r_active");
    	  jQuery(".r06").removeClass("r_active");
    	  jQuery(".r07").removeClass("r_active");
    	  jQuery(".r08").removeClass("r_active");		  
		 });
		 jQuery(".r03").click(function(){
		  jQuery(".r03_act").show();
		  jQuery(".r01_act").hide();
		  jQuery(".r02_act").hide();
		  jQuery(".r04_act").hide();
		  jQuery(".r05_act").hide();
			jQuery(".r06_act").hide();
		    jQuery(".r07_act").hide();
			jQuery(".r08_act").hide();		  
		  //
		  jQuery(".r03").addClass("r_active");
		  jQuery(".r01").removeClass("r_active");
		  jQuery(".r02").removeClass("r_active");
		  jQuery(".r04").removeClass("r_active");
    	  jQuery(".r05").removeClass("r_active");
    	  jQuery(".r06").removeClass("r_active");
    	  jQuery(".r07").removeClass("r_active");
    	  jQuery(".r08").removeClass("r_active");		  
		 }); 
		 jQuery(".r04").click(function(){
		  jQuery(".r04_act").show();
		  jQuery(".r01_act").hide();
		  jQuery(".r03_act").hide();
		  jQuery(".r02_act").hide();
		  jQuery(".r05_act").hide();
			jQuery(".r06_act").hide();
		    jQuery(".r07_act").hide();
			jQuery(".r08_act").hide();		  
		  //
		  jQuery(".r04").addClass("r_active");
		  jQuery(".r01").removeClass("r_active");
		  jQuery(".r03").removeClass("r_active");
		  jQuery(".r02").removeClass("r_active");
    	  jQuery(".r05").removeClass("r_active");
    	  jQuery(".r06").removeClass("r_active");
    	  jQuery(".r07").removeClass("r_active");
    	  jQuery(".r08").removeClass("r_active");		  
		 });
		 jQuery(".r05").click(function(){
		  jQuery(".r05_act").show();
		  jQuery(".r01_act").hide();
		  jQuery(".r03_act").hide();
		  jQuery(".r04_act").hide();
		  jQuery(".r02_act").hide();
			jQuery(".r06_act").hide();
		    jQuery(".r07_act").hide();
			jQuery(".r08_act").hide();		  
		  //
		  jQuery(".r05").addClass("r_active");
		  jQuery(".r01").removeClass("r_active");
		  jQuery(".r03").removeClass("r_active");
		  jQuery(".r04").removeClass("r_active");
    	  jQuery(".r02").removeClass("r_active");
    	  jQuery(".r06").removeClass("r_active");
    	  jQuery(".r07").removeClass("r_active");
    	  jQuery(".r08").removeClass("r_active");		  
		 });
/*---------------------------------------------------*/	
	jQuery(".r06").click(function(){
		  jQuery(".r06_act").show();
		  jQuery(".r01_act").hide();
		  jQuery(".r03_act").hide();
		  jQuery(".r04_act").hide();
		  jQuery(".r02_act").hide();
			jQuery(".r05_act").hide();
		    jQuery(".r07_act").hide();
			jQuery(".r08_act").hide();
		  //
		  jQuery(".r06").addClass("r_active");
		  jQuery(".r01").removeClass("r_active");
		  jQuery(".r03").removeClass("r_active");
		  jQuery(".r04").removeClass("r_active");
    	  jQuery(".r02").removeClass("r_active");
    	  jQuery(".r05").removeClass("r_active");
    	  jQuery(".r07").removeClass("r_active");
    	  jQuery(".r08").removeClass("r_active");		  
		 });	 
	jQuery(".r07").click(function(){
		  jQuery(".r07_act").show();
		  jQuery(".r01_act").hide();
		  jQuery(".r03_act").hide();
		  jQuery(".r04_act").hide();
		  jQuery(".r02_act").hide();
			jQuery(".r05_act").hide();
		    jQuery(".r06_act").hide();
			jQuery(".r08_act").hide();		  
		  //
		  jQuery(".r07").addClass("r_active");
		  jQuery(".r01").removeClass("r_active");
		  jQuery(".r03").removeClass("r_active");
		  jQuery(".r04").removeClass("r_active");
    	  jQuery(".r02").removeClass("r_active");
    	  jQuery(".r06").removeClass("r_active");
    	  jQuery(".r05").removeClass("r_active");
    	  jQuery(".r08").removeClass("r_active");		  
		 });
	jQuery(".r08").click(function(){
		  jQuery(".r08_act").show();
		  jQuery(".r01_act").hide();
		  jQuery(".r03_act").hide();
		  jQuery(".r04_act").hide();
		  jQuery(".r02_act").hide();
			jQuery(".r06_act").hide();
		    jQuery(".r07_act").hide();
			jQuery(".r05_act").hide();		  
		  //
		  jQuery(".r08").addClass("r_active");
		  jQuery(".r01").removeClass("r_active");
		  jQuery(".r03").removeClass("r_active");
		  jQuery(".r04").removeClass("r_active");
    	  jQuery(".r02").removeClass("r_active");
    	  jQuery(".r06").removeClass("r_active");
    	  jQuery(".r07").removeClass("r_active");
    	  jQuery(".r05").removeClass("r_active");
		 });
		 
		
jQuery('.form_amati_mess .close_close img').click(function(){
jQuery('.form_amati_mess').hide(500);
//$('.faq img').show();
	});	
	
	
jQuery('.message_amati').click(function(){
jQuery('.form_amati_mess').show(500);
//$('.faq img').show();
	});	
		 		 
	   </script>

</body>
<?php  }  // конц шаблона для второстепенных страниц
?> 


</html>
